# VCPKG - installations (win-mac)
- windows -> use  powershell (not as admin)

# 

## Install vcpkg.sh
```sh
cd ~/source/cpp                 #windows (or wherever you wish) 
cd ~/bin                        #mac os  --------||----------


git clone https://github.com/Microsoft/vcpkg.git
cd vcpkg
.\bootstrap-vcpkg.bat           # windows
.\bootstrap-vcpkg.sh            # mac-linux 

```

## String for CMake projects 
- CMake projects should use:
- MacOS
  - "-DCMAKE_TOOLCHAIN_FILE=/Users/larsk/bin/vcpkg/scripts/buildsystems/vcpkg.cmake"
- Windows   
    "-DCMAKE_TOOLCHAIN_FILE=C:/Users/larsk/source/cpp/vcpkg/scripts/buildsystems/vcpkg.cmake"

## add this to settings.json (change path to match installation)
```json
 "cmake.configureSettings":{
        "CMAKE_TOOLCHAIN_FILE": "/Users/larsk/bin/vcpkg/scripts/buildsystems/vcpkg.cmake"
    }
```

## VS CODE Extensions
- install c++        (from Microsoft)
- install cmake tool (from Microsoft)

## ## Install package fmt
```sh
# MacOS-> example install fmt (you always get  'x64' )
cd ~/bin/vcpkg && ./vcpkg install fmt   
cd ~/bin/vcpkg && ./vcpkg list 

 .\vcpkg.exe search fmt                  # win -> search
 .\vcpkg.exe install fmt:x64-windows     # win -> install (without 'x64-windows' you will get 32bit)

```

-- return the following cmke instructions
 
    find_package(fmt CONFIG REQUIRED)
    target_link_libraries(main PRIVATE fmt::fmt)

    # Or use the header-only version
    find_package(fmt CONFIG REQUIRED)
    target_link_libraries(main PRIVATE fmt::fmt-header-only)
 
## run code:
```cpp
// main.cpp :  

#include "fmt/format.h"
#include <iostream>

int main(void)
{
    std::string name = "Gitte";
    std::string gitte = fmt::format("The is {}", name);
    std::cout << gitte << endl;
    return 0;
}
```

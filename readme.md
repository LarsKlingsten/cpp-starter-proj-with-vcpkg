# C++ starter project
- cmake
- vcpkg (here using 'fmt' library)
- own libraries
- VS Code 

## program entry -> main()
- let the main() function being the root directory (here starter-proj.cpp), and rest of source code at src 

## Code Runner (@settings.json)
 "cpp": "cd $dir/build  && cmake .. && make &&  ./$fileNameWithoutExt"

 see 
- CMakeLists.txt
- readme-*.md files  

## find packages

https://vcpkg.io/en/packages.html


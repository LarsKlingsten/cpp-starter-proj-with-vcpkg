
#include <iostream>

#include "fmt/format.h"  // 'fmt' library via vcpkg
#include "hello.h"       // our own file via folder ./include

using namespace std;

int main(void) {
   Hello hello;  // test hello.h from ./include and ./src is working
   hello.print();

   string someName = "Peter";
   int someAge = 77;

   // test fmt from vcpkg is working
   string timeFlies = fmt::format("Hallo {}, at {} just {} years to 100!", someName, someAge, 100 - someAge);
   cout << timeFlies << endl;

   string names[] = {"Gitte", "Frederik", "Cecilie!"};

   for (auto name : names) {
      cout << fmt::format("Hallo {}\n", name);
   }

   return 0;
}

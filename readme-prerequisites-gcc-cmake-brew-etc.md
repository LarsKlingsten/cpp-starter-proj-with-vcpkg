# C++ Pre-requisites for using cmake and vcpkg

## MacOS 10.15:
- to use 'VS Code'
```sh
# check, if you got it
brew list cmake --version
brew list gcc   --version
brew list pkg-config --version
which g++                    # should respond /usr/local/bin/g++

# if not then install
brew install cmake
brew install gcc             # to get c++20 (MacOs 10.15 has support for clang++ 11 (max c++17)
brew install pkg-config      # vcpkg depedency

# create symlink gcc (to link brew version as c++ default)
cd /usr/local/bin
ln -s  /usr/local/Cellar/gcc/11.2.0_1/bin/g++-11 g++

# check other symlinks (pointing to gcc/g++)
find /usr/local/bin -iname '*86*'  

# change path
sudo nano /etc/paths # Ensure that '/usr/local/bin' is above (execute before) '/usr/bin'
```

## Windows 10
You need to have following
- Visual Studio 2022 (installer: get c++ tools)
- git                (https://git-scm.com/download/win)


## Tests (on MAC OS):
- test flag "-std=c++20"

```sh 
cd /Users/larsk/source/cpp/learn-cpp/practice-problems/  && \
g++ -std=c++20 findsum.cpp -o /Users/larsk/source/cpp/learn-cpp/practice-problems/bin/findsum -O3 && \
/Users/larsk/source/cpp/learn-cpp/practice-problems/bin/findsum
```